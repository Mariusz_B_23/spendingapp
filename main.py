import os
from decimal import Decimal, getcontext

getcontext().prec = 28

budget = []
spending = []
savings = []


def start():
    print("\nCONTROL OF SPENDING'S APPLICATION")


def show_menu():
    print("\nSelect the option:")
    print("1. Enter the amounts\n2. Show overview of budget")
    print("3. Budget analysis\n4. Export to file")
    print("5. Import from file\n6. Clear data\n0. Exit")


def running():
    while True:
        show_menu()
        user_selection = input("\nEnter your choice:\n---> ")
        match user_selection:
            case '1':
                enter_the_amounts()
            case '2':
                budget_overview()
            case '3':
                budget_analysis()
            case '4':
                export_to_file()
            case '5':
                import_from_file()
            case '6':
                clear_data()
            case '0':
                break
            case _:
                print("\nIncorrect data.")


def enter_the_amounts():
    print("\nEnter the amounts in two categories:")
    daily_budget = return_decimal("Daily budget: ", Decimal('0'))
    budget.append(daily_budget)
    while True:
        daily_spending = return_decimal("Daily spending: ", Decimal('0'))
        if daily_spending > daily_budget:
            print(f"\nDaily spending cannot be more than the daily budget ({daily_budget}). Please enter new amount.\n")
        else:
            spending.append(daily_spending)
            break
    daily_savings = daily_budget - daily_spending
    savings.append(daily_savings)


def return_decimal(prompt, min_value):
    while True:
        try:
            amount = Decimal(input(prompt))
            if amount < min_value:
                print(f"\nSorry, the amount must be greater than or equal to {min_value}.\n")
            else:
                break
        except ValueError:
            print("\nSorry, the amount is not correct.\n")
    return Decimal(amount)


def budget_overview():
    print("\nOverview")
    if len(budget) == 0 or len(spending) == 0 or len(savings) == 0:
        print("\nNo data available.")
    elif len(budget) == len(spending) == len(savings):
        for amount_number, amount in enumerate(budget):
            print(f"{amount_number + 1}. budget: {amount}, "
                  f"spending: {spending[amount_number]}, "
                  f"savings: {savings[amount_number]}")
    else:
        print("The data is not saved correctly.")


def budget_analysis():
    print("\nBudget analysis")
    if len(budget) == 0 or len(spending) == 0 or len(savings) == 0:
        print("\nNo data available for analysis. Enter or import them.")
    else:
        print(f"Total budget:       {sum(budget):.2f}")
        print(f"Total spending:     {sum(spending):.2f}")
        print(f"Total savings:      {sum(savings):.2f}")
        print(f"Average spending:   {sum(spending) / len(spending):.2f}")
        print(f"Average saving:     {sum(savings) / len(savings):.2f}")
        print(f"Maximum spend:      {max(spending):.2f}")
        print(f"Maximum saving:     {max(savings):.2f}")
        print(f"Minimal spend:      {min(spending):.2f}")
        print(f"Minimal saving:     {min(savings):.2f}")
        if len(budget) >= 7:
            show_analysis_for_the_last_seven_days()


def show_analysis_for_the_last_seven_days():
    sum_budget = sum_spending = sum_savings = Decimal('0')
    min_spending = min_savings = Decimal('Infinity')
    max_spending = max_savings = Decimal('-Infinity')
    for amount in range(len(budget) - 7, len(budget)):
        sum_budget += budget[amount]
    for spend in range(len(spending) - 7, len(spending)):
        sum_spending += spending[spend]
        if min_spending > spending[spend]:
            min_spending = spending[spend]
        if max_spending < spending[spend]:
            max_spending = spending[spend]
    for saving in range(len(savings) - 7, len(savings)):
        sum_savings += savings[saving]
        if min_savings > savings[saving]:
            min_savings = savings[saving]
        if max_savings < savings[saving]:
            max_savings = savings[saving]
    print("\nLast seven days")
    print(f"Total budget:       {sum_budget:.2f}")
    print(f"Total spending:     {sum_spending:.2f}")
    print(f"Total savings:      {sum_savings:.2f}")
    print(f"Average spending:   {sum_spending / Decimal('7'):.2f}")
    print(f"Average saving:     {sum_savings / Decimal('7'):.2f}")
    print(f"Maximum spend:      {max_spending:.2f}")
    print(f"Maximum saving:     {max_savings:.2f}")
    print(f"Minimal spend:      {min_spending:.2f}")
    print(f"Minimal saving:     {min_savings:.2f}")


def import_from_file():
    file_name = input("\nEnter the name of the file: ")
    try:
        with open(file_name, "r") as file:
            for line in file:
                amount, spend, saving = line.strip().split(', ')
                budget.append(Decimal(amount))
                spending.append(Decimal(spend))
                savings.append(Decimal(saving))
            print("\nData successfully loaded.")
    except FileNotFoundError:
        print(f"\nFile {file_name} not found.")
    except ValueError:
        print("\nError processing file. Ensure the file is formatted correctly.")


def export_to_file():
    if len(budget) == 0 or len(spending) == 0 or len(savings) == 0:
        print("\nNo data available.")
    elif len(budget) == len(spending) == len(savings):
        file_name = input("Enter the name of the file: ")
        try:
            with open(file_name, "a") as file:
                for amount, spend, saving in zip(budget, spending, savings):
                    formatted_amount = f"{amount:.2f}"
                    formatted_spend = f"{spend:.2f}"
                    formatted_saving = f"{saving:.2f}"
                    file.write(f'{formatted_amount}, {formatted_spend}, {formatted_saving}\n')
            print(f"\nFile {file_name} created.")
        except FileNotFoundError:
            print(f"\nFile {file_name} not found.")
    else:
        print("\nThe data is not saved correctly.")


def clear_data():
    while True:
        print("\nSelect whether you want to delete the data entered or the data file.")
        user_choice = input("\n1. Data\n2. File\n3. Exit\n--->  ")
        match user_choice:
            case "1":
                budget.clear()
                spending.clear()
                savings.clear()
                print("\nData successfully deleted.")
            case "2":
                file_name = input("\nEnter the name of the file to delete: ")
                try:
                    os.remove(file_name)
                    print(f"\nFile {file_name} has been deleted successfully.")
                except FileNotFoundError:
                    print(f"\nThe file {file_name} does not exist.")
                except PermissionError:
                    print(f"\nPermission denied to delete the file {file_name}.")
                except Exception as e:
                    print(f"\nAn error occurred: {e}")
            case "3":
                break
            case _:
                print("\nIncorrect data.")


def end():
    print("\nThank you. I hope you are happy with your budget :)")


start()
running()
end()
